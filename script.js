const body = document.querySelector('body')

function appendToPage(question, output) {
    const div = document.createElement('div')
    const title = document.createElement('h2')
    const ans = document.createElement('p')
    
    title.textContent = question
    ans.textContent = output

    div.appendChild(title)
    div.appendChild(ans)
    
    body.appendChild(div)
}

const q1 = "1. Display number 1 to 20."

function oneToTwenty() {
    let answer = ""
    
    for (let i = 1; i <= 20; i++) {
        answer += i + " "
    }
    
    return answer
}

appendToPage(q1, oneToTwenty())

const q2 = "2. Display the even numbers from 1 to 20."

function evenToTwenty() {
    let answer = ""

    for (let i = 1; i <= 20; i++) {
        if(i % 2 === 0) {
            answer += i + " "
        }
    }

    return answer
}


appendToPage(q2, evenToTwenty())

const q3 = "3. Display the odd numbers from 1 to 20."

function oddToTwenty() {
    let answer = ""

    for (let i = 1; i <= 20; i++) {
        if(i % 2 !== 0) {
            answer += i + " "
        }
    }

    return answer
}


appendToPage(q3, oddToTwenty())

const q4 = "4. Display the multiples of 5 up to 100."

    let answer1 = ""

    for (let i = 5; i <=100; i++) {
        if(i % 5 == 0 ) {
            answer1 = answer1 + i + " "
        }
    }

appendToPage(q4, answer1)

const q5 = "5. Display the square numbers from 1 up to 100."

answer2 = ""
   for (i = 1; i <= 10; i++) {
       answer2 = answer2 + i * i + " "
   }

 appendToPage(q5, answer2)

const q6 = "6. Display the numbers counting backwards from 20 to 1."

 answer3 = ""
   for (let i = 20; i >= 1; i--){
       answer3=answer3 + i + " "
   
    }
   appendToPage(q6, answer3)

   const q7 = "7. Display the even numbers counting backwards from 20 to 1."

anwser4 = ""
   for (let i = 20; i >= 1; i--){
       if(i % 2 == 0)
       anwser4 = anwser4 + i + " "
   
    }
   appendToPage(q7, anwser4)

const q8 = "8. Display the odd numbers from 20 to 1, counting backwards"

answer5 = ""

   for ( i = 20; i >= 1; i--){
   if (i % 2 != 0){
       answer5 = answer5 + i + " "
   }

}
    appendToPage(q8, answer5)

const q9 = "9. Display the multiples of 5, counting down from 100 to 1."

answer6 = ""
   
    for ( i = 100; i >= 5; i--){
    if (i % 5 == 0){
       answer6 = answer6 + i + " "
   }
}

   appendToPage(q9, answer6)

   const q10 ="10. Display the square numbers, counting down from 100. "

   answer7 = ""

   for (i = 10; i >= 1; i--) {
    answer7 = answer7 + i * i + " "
}

appendToPage(q10, answer7)

const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

const q11 = "11. Display the 20 elements of sampleArray. "

    answer8 = ""

    for (let x in sampleArray) {
        answer8 = answer8 + sampleArray[x] + " "   
    }

    appendToPage(q11, answer8)

    const q12 = "12. Display all the even numbers contained in sampleArray."

    anwser9 = ""

    for(let x in sampleArray){
        if(sampleArray[x]%2 == 0){
            anwser9 = anwser9 + sampleArray[x] + " "
        }
    }
    appendToPage(q12, anwser9)

    const q13  = "13. Display all the odd numbers contained in sampleArray. "

    answer10 = ""

    for(let x in sampleArray){
        if(sampleArray[x]% 2 != 0){
            answer10 = answer10 + sampleArray[x] + " "
        }
    }
    appendToPage(q13, answer10)
    
    const q14 = "14. Display the square of each element in sampleArray. "

    anwser11 = ""

    for(let x in sampleArray){
        anwser11 = anwser11 + sampleArray[x] * sampleArray[x] + " "
    }
appendToPage(q14, anwser11)

const q15 = "15. Display the sum of all the numbers from 1 to 20."

answer15 = 0

    for(i = 1; i <= 20; i++){
        answer15 = answer15 + i 
    }
    appendToPage(q15, answer15)
    
    const q16 = "16. Display the sum of all the elements in sampleArray."

    answer16 = 0
    for(let x in sampleArray){
        answer16 = answer16 + sampleArray[x]
    }
    appendToPage(q16, answer16)
    
    const q17 = "17. Display the smallest element in sampleArray."
    
    answer17 = sampleArray[0]
    for(let x in sampleArray){
        if(sampleArray[x] <= answer17){
            answer17 = sampleArray[x]
        }
        console.log(answer17)
    }
    appendToPage(q17, answer17)

    const q18 = "18. Display the largest element in sampleArray."

    answer18 = sampleArray[0]
    for(let x in sampleArray){
        if(sampleArray[x] >= answer18){
            answer18 = sampleArray[x]
        }
    }
    appendToPage(q18, answer18)

    const q19 = "19. Display 20 solid gray rectangles, each 20px high and 100px wide."
    
    answer19 = ""
    appendToPage(q19, answer19)
    for(k=1; k<=20; k++) {var newElement = document.createElement("div");
    newElement.className = "rec";
    newElement.style.width = 100 + "px";
    newElement.style.height = 20 + "px"
    newElement.style.backgroundColor = "gray"
    var newText = document.createTextNode("rec #" + k);
    newElement.appendChild(newText);
    
    // Put the new d1 on the page inside the existing element "d1".
    var destination = document.getElementById("d2");
    destination.appendChild(newElement);
}

const q20 ="20. Display 20 solid gray rectangles, each 20px high, with widths ranging evenly from 105px to 200px (remember #4, above)."

answer20 = ""
    appendToPage(q20, answer20)
    for(q=105; q<=200; q+= 5) {var newElement = document.createElement("d1");
        newElement.className = "cer";
        newElement.style.width = q + "px";
        newElement.style.height = 20 + "px"
        newElement.style.backgroundColor = "gray"
        var newText = document.createTextNode("cer #" + q);
        newElement.appendChild(newText);
        var destination = document.getElementById("d2");
        destination.appendChild(newElement);
    }

    const q21 = "Display 20 solid gray rectangles, each 20px high, with widths in pixels given by the 20 elements of sampleArray."
        
    answer21 = ""
    appendToPage(q21, answer21)
    for(let x in sampleArray) {
        var newElement = document.createElement("d1");
        newElement.className = "bug";
        newElement.style.width = sampleArray[x]+ "px"
        newElement.style.height = 20 + "px"
        newElement.style.backgroundColor = "gray"
        
        var newText = document.createTextNode("bug #" + q);
        newElement.appendChild(newText);
        var destination = document.getElementById("d2");
        destination.appendChild(newElement);
    }
    answer22 = ""
    appendToPage(22)
    for(let x in sampleArray) {
        var newElement = document.createElement("d1");
        newElement.className = "bug";
        newElement.style.width = sampleArray[x]+ "px"
        newElement.style.height = 20 + "px"
        newElement.style.backgroundColor = "gray"
        if(x % 2 == 0){
            newElement.style.backgroundColor = "red"
        }
        var newText = document.createTextNode("bug #" + q);
        newElement.appendChild(newText);
        var destination = document.getElementById("d2");
        destination.appendChild(newElement);
    }
    ansTwentyfour = ""
    
    appendToPage(23)
    for(let x in sampleArray) {
        var newElement = document.createElement("d1");
        newElement.className = "bug";
        newElement.style.width = sampleArray[x]+ "px"
        newElement.style.height = 20 + "px"
        newElement.style.backgroundColor = "gray"
        if(sampleArray[x]%2 == 0){
            newElement.style.backgroundColor = "red"
        }
        var newText = document.createTextNode("bug #" + q);
        newElement.appendChild(newText);
        var destination = document.getElementById("d2");
        destination.appendChild(newElement);
        }


